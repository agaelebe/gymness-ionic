// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var gymness = angular.module('gymness', ['ionic', 'underscore', 'gymness.controllers', 'gymness.services', 'ngCordova', 'timer', 'angular-svg-round-progress']);
var localDB = new PouchDB("gymness");
var remoteDB = new PouchDB("https://ibseenigranctsedgershead:098d718ba1707227c1b46544af49e51a552408f4@gymness.cloudant.com/gymness", {skip_setup: true});

gymness.run(function($ionicPlatform, DatabaseService, $cordovaStatusbar) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
      $cordovaStatusbar.overlaysWebView(true);
      $cordovaStatusbar.styleHex('#673AB7');
    }
  });

})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $stateProvider
  .state('muscle-groups', {
    url: "/muscle-groups",
    templateUrl: "templates/muscle_groups.html",
    controller: 'MuscleGroupCtrl'
  })
  .state('muscle-group-exercises', {
    url: "/muscle-groups/:muscleGroupId",
    templateUrl: "templates/muscle_group_exercises.html",
    controller: 'MuscleGroupCtrl'
  })
  .state('build-workout', {
    url: "/workouts/build",
    templateUrl: "templates/build_workout.html",
    controller: 'BuildWorkoutCtrl'
  })
  .state('edit-workouts', {
    url: "/workouts/edit",
    templateUrl: "templates/list_workouts_to_edit.html",
    controller: 'EditWorkoutsCtrl'
  })
  .state('show-workout', {
    url: "/workouts/edit/:workoutIndex",
    templateUrl: "templates/show_workout.html",
    controller: 'ShowWorkoutCtrl'
  })
  .state('workout-list', {
    url: "/workouts",
    templateUrl: "templates/list_workouts.html",
    controller: 'WorkoutsCtrl'
  })
  .state('workout', {
    url: "/workouts/:workoutIndex",
    templateUrl: "templates/workout.html",
    controller: 'WorkoutCtrl'
  })
    .state('finished-workout', {
    url: "/workouts/finish/:executedWorkoutIndex",
    templateUrl: "templates/finished_workout.html",
    controller: 'FinishedWorkoutCtrl'
  })
  .state('history', {
    url: "/history",
    templateUrl: "templates/history.html",
    controller: 'HistoryCtrl'
  })
    .state('history-executed-workout', {
    url: "/history/:executedWorkoutIndex",
    templateUrl: "templates/show_executed_workout.html",
    controller: 'ShowExecutedWorkoutCtrl'
  })
  .state('home', {
    url: "/home",
    templateUrl: "templates/home.html",
    controller: 'HomeCtrl'
  })
  .state('profile', {
    url: "/profile",
    templateUrl: "templates/profile.html",
    controller: 'ProfileCtrl'
  })
  .state('account', {
    url: "/account",
    templateUrl: "templates/account.html",
    controller: 'AccountCtrl'
  })
  .state('welcome', {
    url: '/welcome',
    templateUrl: "templates/welcome.html",
    controller: 'WelcomeCtrl'
  })
    // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/welcome');

});
