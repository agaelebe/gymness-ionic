angular.module('gymness.controllers', [])

.controller('MuscleGroupCtrl', function($scope, MuscleGroups, exerciseService, $http, $stateParams, $state, $ionicModal){

  $scope.muscleGroups = [];

  MuscleGroups.all().then(function(res){
    $scope.muscleGroups = res;
    $scope.activeMuscleGroup = $scope.muscleGroups[$stateParams.muscleGroupId];
  });

  $scope.addExercises = function() {

    exerciseService.addSelectedExercises($scope.activeMuscleGroup);
    MuscleGroups.setSelectedExercises($scope.activeMuscleGroup, $stateParams.muscleGroupId);

    $state.go('build-workout', {}, {reload: true});

  };

  $scope.getMuscleGroupClass = function(muscleGroup) {
    if (exerciseService.muscleGroupHasExercises(muscleGroup.name) === true) {
      return "item item-content item-has-exercises";
    }
    else {
      return "item item-content";
    }
  };

   $ionicModal.fromTemplateUrl('templates/exercise-modal.html', function($ionicModal) {
    $scope.modal = $ionicModal;

  }, {
    // Use our scope for the scope of the modal to keep it simple
    scope: $scope,
    // The animation we want to use for the modal entrance
    animation: 'slide-in-up'
  });


  $scope.openModal = function(exercise) {
    $scope.exercise = exercise;
    $scope.modal.show();

    $scope.toggleImage = function(exercise) {
      if (exercise.images != undefined) {
        exercise.images.reverse();
      }
    }

  };


})

.controller('HomeCtrl', function($scope, $cordovaGoogleAnalytics, $ionicPlatform, $state, UserService){

  UserService.getUser().then(function(user) {
    $scope.user = user;
  });

  $scope.pictureURL = function() {
    if ($scope.user !== undefined) {
      console.log($scope.user);
      return $scope.user.picture;
    } else {
      return './img/gymness.png';
    }
  }

  //$cordovaGoogleAnalytics.trackView("Home controller");


  // On home screen always exit the app when back button is pressed
  $ionicPlatform.onHardwareBackButton(function () {
    if($state.current.name == "home"){
      ionic.Platform.exitApp();
    }
  }, 101);

})

.controller('WorkoutsCtrl', function($scope, workoutListService) {

  $scope.workout_list = {};

  workoutListService.getWorkoutList().then(function(workouts) {
    $scope.workout_list = workouts;
  })
})


.controller('ShowWorkoutCtrl', function($scope, $stateParams, workoutService) {

  $scope.workout = {};

  console.log($scope.workout);

  workoutService.getWorkout($stateParams.workoutIndex).then(function(workout) {
    $scope.workout = workout;
    $scope.workout_date = function(date) {
      moment.locale('pt-BR');
      return moment(date).format('L');
    };

  });


})

.controller('AccountCtrl', function($scope, DatabaseService, UserService, IdService, $ionicPopup, $state, $ionicLoading) {

  UserService.getUser().then(function(user) {
    $scope.user = user;
  });


  $scope.showLogOutMenu = function() {

    var confirmPopup = $ionicPopup.confirm({
      title: 'Desconectar do Facebook',
      template: 'Tem certeza que deseja desconectar?</br>Seus dados do Gymness serão removidos desse dispositivo.',
      cancelText: 'Cancelar',
      okText: 'Sim',
      okType: 'button-royal'
    });

    confirmPopup.then(function(res) {
      if(res) {
        $ionicLoading.show({template: 'Saindo...'});

        facebookConnectPlugin.logout(function(){
          $ionicLoading.hide();
          localDB.destroy().then(function() {
            // create a new userId
            IdService.resetUserId();

            // recreate the localDB
            localDB = new PouchDB("gymness");

            DatabaseService.initDb(IdService.setUserId());
            $state.go('welcome');
          });
          },
          function(fail){
            console.log('facebook logout fail');
            $state.go('home');
            $ionicLoading.hide();
        });

      } else {
        console.log('content load error');
      }
   });
  }

})


.controller('EditWorkoutsCtrl', function($scope, $state, exerciseService, MuscleGroups, workoutListService) {
  $scope.workout_list = [];
  $scope.loading = true;

  $scope.showEmptyMessage = function(){
    if($scope.workout_list != undefined) {
        return ($scope.workout_list.length === 0);
      }
  };

  workoutListService.getWorkoutList().then(function(workouts) {
    $scope.workout_list = workouts;
    $scope.loading = false;
  }).finally(function () {
      // Hide loading spinner whether our call succeeded or failed.
      $scope.loading = false;
    });

  $scope.data = {
    showDelete: false
  };

  $scope.onWorkoutDelete = function(workout) {
    $scope.workout_list.splice($scope.workout_list.indexOf(workout), 1);
    workoutListService.removeWorkoutFromList(workout._id);
  };

  $scope.newWorkout = function() {
    MuscleGroups.cleanExercises();
    exerciseService.cleanExercises();

    $state.go('muscle-groups', {}, {reload: true});
  };

})

.controller('ProfileCtrl', function($scope, $state, ProfileService) {
  $scope.profile = {};

  ProfileService.getProfile().then(function(profile) {
    $scope.profile = profile;
  });

  $scope.saveProfile = function(profile) {
    ProfileService.setProfile(profile);
    $state.go('home');
  }
})

.controller('HistoryCtrl', function($scope, workoutListService) {
  $scope.workout_history_list = [];


  $scope.showEmptyMessage = function(){
    return ($scope.workout_history_list.length === 0);
  };

  workoutListService.getExecutedWorkoutList().then(function(executedWorkoutList) {
    $scope.workout_history_list = executedWorkoutList;
    $scope.grouped_workout_history_list = _.chain(executedWorkoutList).groupBy('date').pairs().sortBy(0).value();
  });

  $scope.workout_time = function(date) {
    moment.locale('pt-BR');
    return moment(date).format('H:mm');
  }

})


.controller('BuildWorkoutCtrl', function($scope, $cordovaGoogleAnalytics, $state, $ionicPopup, $ionicPlatform, MuscleGroups, exerciseService, suggestedColorService, suggestedTitleService) {

  //$cordovaGoogleAnalytics.trackView("Build Workout Controller");


  $scope.selectedExercises = exerciseService.getExercises();

   // $scope.$on('selectedList:cleaned', function(event,data) {
   //  $scope.selectedExercises = [];
   // });

  $scope.data = {};

  $scope.remove = function(selectedExercises, index, parentIndex){
    MuscleGroups.uncheckExercise(selectedExercises[parentIndex].name, selectedExercises[parentIndex].exercises[index].id);

    selectedExercises[parentIndex].exercises.splice(index, 1);

    // If no exercises are left also remove the MuscleGroup
    if (selectedExercises[parentIndex].exercises.length === 0) {
      selectedExercises.splice(parentIndex, 1);
    }
  };

  $scope.workoutStatus = function() {
    return exerciseService.getTotalExercises();
  };

  $scope.hasExercises = function() {
    return (exerciseService.getTotalExercises() > 0);
  };

  $scope.completeWorkout = function() {

    $scope.data.title = suggestedTitleService.getTitle();

    suggestedTitleService.getTitle().then(function(title) {
      $scope.data.title = title;
    })

    suggestedColorService.getColor().then(function(color) {
      $scope.data.color = color;
    })

    var myPopup = $ionicPopup.show({
      templateUrl: 'templates/save-workout-modal.html',
      title: 'Digite o título do treino',
      subTitle: '',
      scope: $scope,
      buttons: [
        { text: 'Cancelar' },
        {
          text: '<b>Salvar</b>',
          type: 'button-royal',
          onTap: function(e) {

            if (!$scope.data.title) {
              //don't allow the user to close unless he enters the title
              e.preventDefault();
            } else {
              MuscleGroups.cleanExercises();
              exerciseService.saveWorkout($scope.selectedExercises, $scope.data.title, $scope.data.color);
              return $state.go('home');;
            }

          }
        }
      ]
    });

  }


})

.controller('WorkoutCtrl', function($scope, $cordovaGoogleAnalytics, $state, $q, $ionicModal, $ionicPopup, $ionicHistory, $stateParams, $ionicPlatform, workoutService) {

  //$cordovaGoogleAnalytics.trackView("Workout controller - executing a workout");

  workoutService.executeWorkout($stateParams.workoutIndex).then(function(workout) {
    $scope.executed_workout = workout;
  });

  $scope.timerRunning = true;

  $scope.startTimer = function (){
      $scope.$broadcast('timer-start');
      $scope.timerRunning = true;
  };

  $scope.startTimer();

  // $scope.stopTimer = function (){
  //     $scope.$broadcast('timer-stop');
  //     $scope.timerRunning = false;
  // };

  // $scope.$on('timer-stopped', function (event, data){
  //     console.log('Timer Stopped - data = ', data);
  // });


  // Load the modal from the given template URL
  $ionicModal.fromTemplateUrl('templates/edit-workout-modal.html', function($ionicModal) {
    $scope.modal = $ionicModal;

    $scope.modal.decrement = function(value, min) {
      if ($scope.set[value] <= min) { return; }
      $scope.set[value] -= 1;
    };

    $scope.modal.increment = function(value, max) {
      if ($scope.set[value] >= max) { return; }
      $scope.set[value] += 1;
    };


  }, {
    // Use our scope for the scope of the modal to keep it simple
    scope: $scope,
    // The animation we want to use for the modal entrance
    animation: 'slide-in-up'
  });

  $scope.openModal = function(set) {
    $scope.set = set;
    $scope.modal.show();
  };

  $scope.WorkoutDurationToShort = function(started_at, seconds) {
    totalTime = (new Date()).getTime() - started_at;

    console.log(totalTime);

    if (totalTime < (seconds*1000)) {
      return true;
    } else {
      return false;
    }
  };


  $scope.saveCurrentWorkout = function(executed_workout) {
    workoutService.saveCurrentWorkout(executed_workout);

    // Don't go back to this view
    $ionicHistory.currentView($ionicHistory.backView());

    console.log(executed_workout._id);
    $state.go('finished-workout', {executedWorkoutIndex: executed_workout._id});

  }

  $scope.cancelWorkoutConfirmation = function() {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Cancelar treino',
       template: 'Tem certeza que deseja encerrar o treino sem salvar seu progresso?',
       cancelText: 'Não',
       okText: 'Sim',
       okType: 'button-royal'
     });
     confirmPopup.then(function(res) {
       if(res) {
        console.log('cancela treino');
        $state.go('home');
       } else {
         console.log('continua treino');
       }
     });
   };

  $scope.finishWorkoutConfirmation = function() {

    if($scope.WorkoutDurationToShort($scope.executed_workout.started_at, 300)) {

      var confirmPopup = $ionicPopup.confirm({
         title: 'Finalizar treino',
         template: 'Seu treino foi muito rápido. Tem certeza que deseja finalizar o treino?',
         cancelText: 'Não',
         okText: 'Sim',
         okType: 'button-royal'
       });

      confirmPopup.then(function(res) {
       if(res) {
        $scope.saveCurrentWorkout($scope.executed_workout);
        console.log('finaliza treino curto');

       } else {
         console.log('continua treino curto');
       }
     });
    } else {
      console.log('finaliza treino longo');
      $scope.saveCurrentWorkout($scope.executed_workout);
    }

   };

})

.controller('FinishedWorkoutCtrl', function($scope, $cordovaGoogleAnalytics, $ionicHistory, $state, $stateParams, $ionicLoading, $ionicPlatform, $cordovaSocialSharing, workoutService, WorkoutDurationService  ) {

  //$cordovaGoogleAnalytics.trackView("Workout controller - executing a workout");


  workoutService.getWorkout($stateParams.executedWorkoutIndex).then(function(workout) {
    $scope.executed_workout = workout;
    $scope.workout_duration = WorkoutDurationService.getDuration($scope.executed_workout.started_at, $scope.executed_workout.ended_at);
    //$ionicLoading.hide();

    $scope.max = $scope.executed_workout.total_exercises;
    $scope.current = $scope.executed_workout.completed_exercises;

    // share anywhere
    $scope.shareWorkout = function() {

      message_title = "Terminei um treino usando o Gymness!";
      message_body =  "Realizei " + $scope.executed_workout.completed_exercises + " exercício(s) em " + $scope.workout_duration + "."

      $cordovaSocialSharing.share(message_body, message_title, null, 'http://www.gymness.com.br');
    }

  })

})

.controller('ShowExecutedWorkoutCtrl', function($scope, $stateParams, workoutService, WorkoutDurationService) {

  $scope.executed_workout = {};

  $scope.filterMuscleGroupsWithCompletedExercises = function(item) {
      function isNotCompleted(element, index, array) {
        return element.completed === false;
      }
      return item.exercises.every(isNotCompleted) === false;
  };


  workoutService.getWorkout($stateParams.executedWorkoutIndex).then(function(workout) {
    $scope.executed_workout = workout;
    $scope.workout_duration = WorkoutDurationService.getDuration($scope.executed_workout.started_at, $scope.executed_workout.ended_at);
  })

  $scope.workout_date = function(date) {
    moment.locale('pt-BR');
    return moment(date).format('L') + ' às ' +  moment(date).format('H:mm');
  }

  $scope.workout_summary = function(date) {
    moment.locale('pt-BR');
    return moment(date).format('L');
  };

})

.controller('WelcomeCtrl', function($scope, $cordovaGoogleAnalytics, $state, $q, $ionicLoading, DatabaseService, IdService, UserService) {

  function redirectIfAlreadySignedIn() {
    UserService.getUser().then(function(currentUser) {
      console.log(currentUser);
      console.log(currentUser._id);
      if (currentUser && currentUser.facebook_user_id) {
        console.log('go home');
        $state.go('home');
      }
    })
  }

  redirectIfAlreadySignedIn();

  $scope.noSignIn = function() {

    var userId = IdService.getUserId();

    if (!userId) {
      userId = IdService.setUserId();
    }

    DatabaseService.initDb(userId);
    $state.go('home');
  };


  function _waitForAnalytics(){
      if(typeof analytics !== 'undefined'){
        $cordovaGoogleAnalytics.debugMode();
        $cordovaGoogleAnalytics.startTrackerWithId("UA-71200265-1");
        $cordovaGoogleAnalytics.trackView("Welcome controller - first screen");
      }
      else{
          setTimeout(function(){
              _waitForAnalytics();
          },500);
      }
  };
  _waitForAnalytics();


  //This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse).then(function(profileInfo) {

      IdService.getUserIdForFacebook(profileInfo.id).then(function(userId) {

        console.log('userId in gymness db associtated with facebook_user_id ' + profileInfo.id);
        console.log(userId);

        // set user in local db

        var currentUserId = IdService.setUserId(userId);
        console.log('currentUserId');
        console.log(currentUserId);
        DatabaseService.initDb(currentUserId);

        if (!userId) {
          UserService.setUser({
          authResponse: authResponse,
          facebook_user_id: profileInfo.id,
          name: profileInfo.name,
          email: profileInfo.email,
          picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
          });
        }

      });

      $ionicLoading.hide();
      $state.go('home');
    }, function(fail){
      //fail get profile info
      console.log('profile info fail in the success callback', fail);
    });
  };

  //This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  //this method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function(authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
        console.log(response);
        info.resolve(response);
      },
      function (response) {
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookSignIn = function() {

    facebookConnectPlugin.getLoginStatus(function(success){
     if(success.status === 'connected'){
        // the user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);

        //check if we have our user saved

           getFacebookProfileInfo(success.authResponse).then(function(profileInfo) {

            //User has already logged with facebook before
            console.log('already connected');

            IdService.getUserIdForFacebook(profileInfo.id).then(function(userId) {

              console.log('userId in gymness db associtated with facebook_user_id ' + profileInfo.id);
              console.log(userId);

              var currentUserId = IdService.setUserId(userId);
              DatabaseService.initDb(currentUserId);

              if (!userId) {
                UserService.setUser({
                authResponse: authResponse,
                facebook_user_id: profileInfo.id,
                name: profileInfo.name,
                email: profileInfo.email,
                picture : "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large"
                });
              }

            });

            //$ionicLoading.hide();
            $state.go('home');
          }, function(fail){
            //fail get profile info
            console.log('profile info fail in the success callback', fail);
          });


     } else {
        //if (success.status === 'not_authorized') the user is logged in to Facebook, but has not authenticated your app
        //else The person is not logged into Facebook, so we're not sure if they are logged into this app or not.
        console.log('getLoginStatus', success.status);

        $ionicLoading.show({
         template: 'Entrando...'
        });

        //ask the permissions you need. You can learn more about FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };
})

