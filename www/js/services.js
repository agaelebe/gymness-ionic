angular.module('gymness.services', [])


.service('DatabaseService', ['IdService', function(IdService) {

  //var localDB = new PouchDB("gymness");
  //var remoteDB = new PouchDB("https://ibseenigranctsedgershead:098d718ba1707227c1b46544af49e51a552408f4@gymness.cloudant.com/gymness", {skip_setup: true});

  var initDb = function(userId) {


    console.log('InitDB with userID');
    console.log(userId);

   localDB.sync(remoteDB, {
        live: true,
        retry: true,
        filter: 'app/by_user_id',
        query_params: { "user_id": userId }
          }).on('change', function (change) {
            // yo, something changed!
            console.log('something changed');
          }).on('paused', function (info) {
            // replication was paused, usually because of a lost connection
            console.log('paused');
          }).on('active', function (info) {
            console.log('active');
            // replication was resumed
          }).on('error', function (err) {
            // totally unhandled error (shouldn't happen)
            console.log('error in sync');
          });
  };

  return {
    initDb: initDb
  };

}])

.service('UserService', ['$q', 'IdService', function($q, IdService) {

//for the purpose of this example I will store user data on ionic local storage but you should save it on a database

  var getUser = function(){
    console.log('get user');
    var userId = IdService.getUserId();
    console.log(userId);
    return $q.when(localDB.get(userId));
  };

  var setUser = function(user_data){
    var userId = IdService.getUserId();
    user_data['user_id'] = userId;
    return $q.when(localDB.put(user_data, userId));
  };

  // var setUser = function(user_data) {
  //   window.localStorage.starter_facebook_user = JSON.stringify(user_data);
  // };

  // var getUser = function(){
  //   return JSON.parse(window.localStorage.starter_facebook_user || '{}');
  // };

  return {
    getUser: getUser,
    setUser: setUser
  };
}])

.service('IdService', ['$cordovaDevice', '$q', function($cordovaDevice, $q) {

  var generateUUID = function() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
  };

  var getUserId = function() {
    // try{
    //   return $cordovaDevice.getUUID();
    // }
    // catch(err){
    //   console.log(err.message);
    //   return "testUUID";
    //   }
    return window.localStorage['userId'];
  };

  var setUserId = function(id) {
    var userId = id || ('user-' + generateUUID());
    console.log('setUserId');
    console.log(userId);
    window.localStorage['userId'] = userId;
    return userId;
  }

  var resetUserId = function() {
   window.localStorage['userId'] = null;
   return true;
  }

  var getUserIdForFacebook = function(facebookProfileId) {
    return $q.when(remoteDB.query('facebook/facebook-users-view',
    {
      key: facebookProfileId,
      include_docs: true,
      limit: 1
    })).then(function (result) {
      if (result.rows.length > 0) {
        console.log('found ' + result.rows[0].doc.user_id);
        return result.rows[0].doc._id;
      } else {
        // no user found
        console.log(' not found');
        return undefined;
      }
    }).catch(function(err) { console.log(err); });
  }


  return {
    generateUUID: generateUUID,
    getUserId: getUserId,
    getUserIdForFacebook: getUserIdForFacebook,
    setUserId: setUserId,
    resetUserId: resetUserId
  }

}])

.service('ProfileService', ['$q' ,'$cordovaDevice', 'IdService', function($q, $cordovaDevice, IdService) {

  var userId = IdService.getUserId();

  var getProfile = function(){
    return $q.when(localDB.get('profile-' + userId));
  };

  var setProfile = function(profile){
    profile['user_id'] = userId;
    return $q.when(localDB.put(profile, 'profile-' + userId));
  };

  return {
    getProfile: getProfile,
    setProfile: setProfile
  };

}])

.service('workoutListService', ['$q', function($q) {

  var getWorkoutList = function(){
    var result =  $q.when(localDB.allDocs({
          include_docs: true,
          attachments: true,
          startkey: 'workout',
          endkey: 'workout\uffff'
          })).then(function(result) {
              workouts = result.rows.map(function(row) {
                return row.doc;
              });

            return workouts;
          }).catch(function(err) {
            console.log(err);
      });
      return result;
  };


  var removeWorkoutFromList = function(workout_id) {
    return localDB.get(workout_id)
                  .then(function (doc) {
                          return localDB.remove(doc);
    });
  };

  var getExecutedWorkoutList = function(){
    var result =  $q.when(localDB.allDocs({
          include_docs: true,
          attachments: true,
          startkey: 'executedWorkout',
          endkey: 'executedWorkout\uffff'
          })).then(function(result) {
              workouts = result.rows.map(function(row) {
                return row.doc;
              });

            return workouts;
          }).catch(function(err) {
            console.log(err);
      });
      return result;
  };

  return {
    getWorkoutList: getWorkoutList,
    removeWorkoutFromList: removeWorkoutFromList,
    getExecutedWorkoutList: getExecutedWorkoutList
   };


}])

.service('workoutService', ['$q', 'IdService', function($q, IdService) {


  var userId = IdService.getUserId();

  var getWorkout = function(workout_id) {
    return $q.when(localDB.get(workout_id));
  };

  var setExecutedWorkout = function(executedWorkout) {
    return $q.when(localDB.put(executedWorkout));
  };

  var executeWorkout = function(workout_id){

    moment.locale('pt-BR');
    var date = new Date();

    var executedWorkout = {
      _id: 'executedWorkout-' + IdService.generateUUID(),
      workout_title: '',
      workout_color: undefined,
      workout: [],
      started_at: date.getTime(),
      ended_at: undefined,
      date: moment(date).format('L'),
      completed_exercises: 0,
      total_exercises: 0,
      percentage_completed: 0,
      user_id: userId
    };

    return getWorkout(workout_id).then(function(currentWorkout) {

      executedWorkout.workout_title = currentWorkout.title;
      executedWorkout.workout_color = currentWorkout.color;

      currentWorkout.workout.forEach(function(muscleGroup, index) {
        var blankMuscleGroup = {
            name: muscleGroup.name,
            exercises: []
          }

        executedWorkout.workout[index] = blankMuscleGroup;


        muscleGroup.exercises.forEach(function(exercise) {

          var new_exercise = {
            name: exercise.name,
            id: exercise.id,
            set: exercise.set,
            sets: [],
            repetitions: exercise.repetitions,
            weight: exercise.weight,
            completed: false
          };

          for (var i = 1; i <= exercise.set; i++) {
            new_exercise.sets.push({
              name: exercise.name,
              current_set: i,
              repetitions: exercise.repetitions,
              weight: exercise.weight,
              completed: false
              });
          }

          executedWorkout.workout[index].exercises.push(new_exercise);
      });
    });

    return executedWorkout;

    });
  };

  var saveCurrentWorkout = function(executedWorkout){
    executedWorkout.ended_at = (new Date()).getTime();

    var totalExercises = 0;
    var completed = 0;
    for(var i=0; i<executedWorkout.workout.length; i++){
      for(var j=0; j<executedWorkout.workout[i].exercises.length; j++){
        totalExercises++;
        if(executedWorkout.workout[i].exercises[j].completed == true) {
          completed++;
        }
      }
    }

    executedWorkout.completed_exercises = completed;
    executedWorkout.total_exercises = totalExercises;




    setExecutedWorkout(executedWorkout);

  };

  var cancelCurrentWorkout = function(){
    return false;
  };

  var getGetLastExecutedWorkout = function() {

  };

  return {
    getWorkout: getWorkout,
    executeWorkout: executeWorkout,
    saveCurrentWorkout: saveCurrentWorkout,
    cancelCurrentWorkout: cancelCurrentWorkout,
    getGetLastExecutedWorkout: getGetLastExecutedWorkout
   };

}])

.service('exerciseService', ['$q', 'IdService', 'validateTitleService', function($q, IdService, validateTitleService) {
  var selectedList = [];

  var cleanExercises = function() {
    selectedList.splice(0, selectedList.length);
    return selectedList;
  }

  var getExercises = function(){
    return selectedList;
  };

  var getTotalMuscleGroups = function(){
    return selectedList.length;
  };

  var getTotalExercises = function(){
    var totalExercises = 0;
    selectedList.forEach(function(muscleGroup) {
      totalExercises += muscleGroup.exercises.length;
    });
    return totalExercises;
  };

  var muscleGroupHasExercises = function(muscleGroupName) {
    for (var i = 0; i < selectedList.length; i++) {
      if (selectedList[i].name === muscleGroupName) {
        return true;
      }
    }
    return false;
  };

  var addSelectedExercises = function(muscleGroup) {

    var selectedExercises = muscleGroup.exercises.filter(function(obj) {
      if (typeof(obj.checked) !== 'undefined' && obj.checked == true) {
        return true;
      } else {
        return false;
      }
    });

    var muscleGroupExercises = {
      name: muscleGroup.name,
      exercises: selectedExercises
    }

    var muscleGroupAlreadyAdded = selectedList.some(function(element, index, array) {
      if (typeof(element.name) !== 'undefined' && (element.name == muscleGroupExercises.name)) {
        return true;
      }
      return false;
    });

    if (muscleGroupAlreadyAdded == true) {
      selectedList.forEach(function(muscleGroup) {

        if (muscleGroup.name === muscleGroupExercises.name) {
          muscleGroup.exercises = muscleGroupExercises.exercises;
        }

        if (muscleGroup.exercises.length == 0) {
          delete muscleGroup;
        }

      });
    } else {
      selectedList.push(muscleGroupExercises);
    }

    // Remove empty MuscleGroups
    for (var i = selectedList.length-1; i >= 0; i--) {
      if (selectedList[i].exercises.length === 0) {
          selectedList.splice(i, 1);
      }
    }

    return selectedList;
  };

  var saveWorkout = function(exercises, title, color) {

      var id = 'workout-' + IdService.generateUUID();
      var workout = {
          _id: id,
          title: title,
          color: color,
          workout: exercises,
          created_at: new Date().getTime(),
          user_id: IdService.getUserId()
        };

      return $q.when(localDB.put(workout));


  };

  return {
    addSelectedExercises: addSelectedExercises,
    muscleGroupHasExercises: muscleGroupHasExercises,
    cleanExercises: cleanExercises,
    getExercises: getExercises,
    saveWorkout : saveWorkout,
    getTotalMuscleGroups: getTotalMuscleGroups,
    getTotalExercises: getTotalExercises
  };

}])

.service('suggestedColorService', function(workoutListService){

  var colors = [
    'positive',
    'calm',
    'balanced',
    'energized',
    'assertive'
  ];

  // var colors = [
  //   { name: 'vermelho',
  //     code: '#D32F2F'
  //   },
  //   {
  //     name: 'azul',
  //     code: '#536DFE'
  //   },
  //   {
  //     name: 'verde',
  //     code: '#388E3C'
  //   },
  //     name: 'laranja',
  //     code: '#FF9800'
  // ];


  var getColor = function(muscleGroup) {

    return workoutListService.getWorkoutList().then(function(workoutList) {

      var workoutListColors = [].map.call(workoutList, function(obj) {
        return obj.color;
      });

      for (var i = 0; i < colors.length; i++) {
        if (workoutListColors.indexOf(colors[i]) === -1) {
          return colors[i];
        }
      }
      return "";
    })
  };

  return {
    getColor: getColor
  };



})


.service('validateTitleService', ['workoutListService', function(workoutListService) {

  // This is a first trial, not working as expected

  var MAX_WORKOUTS_NUMBER = 100;

  var avoidRepeatedTitle = function(title) {

    var validatedTitle = title;

    validatedTitle = workoutListService.getWorkoutList().then(function(workoutList) {

      var validated = title;

      var workoutListTitles = [].map.call(workoutList, function(obj) {
        return obj.title;
      });

      console.log(workoutListTitles);

      for (var i = 0; i < workoutListTitles.length; i++) {
        if(workoutListTitles[i] === validated) {

          var titleNumber = title.substr(validated.length - 3);
          console.log(titleNumber);

          for(var j = 1; j < MAX_WORKOUTS_NUMBER; j++) {
            if(titleNumber !== ("(" + j.toString() + ")")) {
              validated =  (title + " (" + j.toString() + ")");
              break;
            }
          }
        }
        console.log('validated correct: ' + validated);
      }

      return validatedTitle;

    });

    return validatedTitle;
  }

  return {
    avoidRepeatedTitle: avoidRepeatedTitle
  };

}])

.service('suggestedTitleService', function(workoutListService) {

  var workoutTitles = [
    "Treino A",
    "Treino B",
    "Treino C",
    "Treino D",
    "Treino E"
  ];

  var getTitle = function(muscleGroup) {

    return workoutListService.getWorkoutList().then(function(workoutList) {

      var workoutListTitles = [].map.call(workoutList, function(obj) {
        return obj.title;
      });

      for (var i = 0; i < workoutTitles.length; i++) {
        if (workoutListTitles.indexOf(workoutTitles[i]) === -1) {
          return workoutTitles[i];
        }
      }
      return "";
    })
  };

  return {
    getTitle: getTitle
  };

})

.service('WorkoutDurationService', function() {

  this.getDuration = function(started_at, ended_at) {

    var durationInSeconds = moment(ended_at).diff(moment(started_at),'seconds');
    var durationInMinutes = moment(ended_at).diff(moment(started_at),'minutes');

    if (durationInMinutes > 60) {
      var durationDiff = moment(ended_at).diff(moment(started_at));
      var d = moment.duration(durationDiff);
      return Math.floor(d.asHours()) + moment.utc(d).format(":mm:ss");
    }

    if (durationInMinutes === 0) {
      return durationInSeconds + ' segundo(s)';
    } else {
      return durationInMinutes + ' minuto(s)';
    }
  };

})

.factory('MuscleGroups', ['$http', '_', function($http, _) {
  var groups = [];

  function getAll() {
      // return [
      //   {
      //     name: 'teste',
      //     exercises: ['supino', 'supino inclinado', 'barra']
      //   },
      //   {
      //     name: 'teste 2',
      //     exercises: ['supino 2', 'supino inclinado 3', 'barra 4']
      //   }
      // ];
         return $http.get('json/exercises.json').then(function(json){
            if(groups.length === 0) {
              groups = json.data;
            }
            return groups;
          });
  };

  return {
    all: getAll
    ,
    cleanExercises: function() {
      //groups.splice(0, groups.length);
      groups.forEach(function(muscleGroup, index) {
        muscleGroup.exercises.forEach(function(exercise, exerciseIndes){
          exercise['checked'] = false;
        });
      });
    },

    uncheckExercise: function(muscleGroupName, exerciseId) {

      var muscleGroup = _.chain(groups).where({name: muscleGroupName}).first().value();
      var exercises = muscleGroup.exercises;

      var muscleGroupIndex = groups.findIndex(function(group) {
        return group['name'] == muscleGroupName;
      });
      var exerciseIndex = exercises.findIndex(function(exercise) {
        return exercise['id'] == exerciseId;
      });

      groups[muscleGroupIndex].exercises[exerciseIndex]['checked'] = false;

    },

    setSelectedExercises: function(muscleGroup, muscleGroupId) {
      groups[muscleGroupId] = muscleGroup;
    },
    getLastActiveIndex: function() {
      return parseInt(window.localStorage['lastActiveMuscleGroup']) || 0;
    },
    setLastActiveIndex: function(index) {
      window.localStorage['lastActiveMucleGroup'] = index;
    }
  }
}])

